const ImageProcessor = require('./imageProcessor');

try{
    let ProcessTestDataset = (ok)=>{
        if(ok){
            Esper.processDataset('testdata')
                .then(()=>{
                    Esper.done();
                    console.log("Processed successfully");
                    process.exit(0);
                })
                .catch((e)=>{
                    Esper.done();
                    console.log(`Error: ${e}`);
                    process.exit(3);
                })
        }else{
            process.exit(2);
        }
    }
    let Esper = new ImageProcessor('192.168.11.1',ProcessTestDataset);
}catch(e){
        process.exit(1);
}
