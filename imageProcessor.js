const { io } = require("socket.io-client");
class ImageProcessor{
    #ip
    #io
    #timeout
    #datasetsUpdated

    constructor(imageProcessorMasterIP = '192.168.11.1',callback=()=>{}) {
        this.#ip = imageProcessorMasterIP;
        let wsAddr = `http://${this.#ip}:4000`;
        console.log(`connecting to ${wsAddr}`);
        this.#io = io(wsAddr, { withCredentials: false });
        this.#timeout = setTimeout(()=>{
            this.done();
            console.error("could not connect to image processing master ");
            callback(false);
        },5000);
        this.datasets =[];
        this.#datasetsUpdated = (dataset)=>{}
        this.#io.on('connect',()=>{
            clearTimeout(this.#timeout);
            this.#io.emit("client-registration",'api');
            console.log("connected to image processing master");
            setTimeout(()=>{callback(true)},500);
        })
        this.#io.on('data-available',(ds)=>{
            this.datasets = ds;
            this.#datasetsUpdated(ds);
        });
    }

    allPossibleMaps = ['parallel','neutral','cross','normal','specular','tangent'];

    possibleBaseImages = ['parallel','neutral','cross'];

    registerDatasetUpdateCallback(callbackFunction){
        console.log(`Dataset update callback function registered`);
        this.#datasetsUpdated=callbackFunction;
    }
    processDataset(datasetName,baseImage='parallel',doAlign=true,mapsToProduce = ['parallel','neutral','cross','normal','specular','tangent']){
        return new Promise((resolve,reject)=>{
            let found = false;
            let index;
            for (let i = 0; i <this.datasets.length ; i++) {
                if(this.datasets[i].name === datasetName){
                    found = true;
                    index = i;
                }
            }
            if(found){
                let mapsNamedCorrectly = true;
                let incorrectNames = [];
                let mapsCanBeMade = true;
                let impossibleMaps = [];
                for (let i = 0; i < mapsToProduce.length; i++) {
                    if(this.allPossibleMaps.includes(mapsToProduce[i])){
                        if(!this.datasets[index].possibleMaps.includes(mapsToProduce[i])){
                            mapsCanBeMade=false;
                            impossibleMaps.push(mapsToProduce[i]);
                        }
                    }else{
                        mapsNamedCorrectly=false;
                        incorrectNames.push(mapsToProduce[i]);
                    }
                }
                if(mapsCanBeMade){
                    if(this.possibleBaseImages.includes(baseImage)){
                        let baseImageIndex = -1;
                        let ok = true;
                        switch(baseImage){
                            case 'parallel':
                                baseImageIndex = this.datasets[index].imageLocations.gi_parallel;
                                if( baseImageIndex === -1){
                                    ok=false;
                                    reject(`no gi_parallel for dataset ${datasetName} - cannot use parallel base image`);
                                }
                                break;
                            case 'neutral':
                                baseImageIndex = this.datasets[index].imageLocations.gi_neutral;
                                if( baseImageIndex === -1){
                                    ok=false;
                                    reject(`no gi_neutral for dataset ${datasetName} - cannot use neutral base image`);
                                }

                                break;
                            case 'cross':
                                baseImageIndex = this.datasets[index].imageLocations.gi_cross;
                                if( baseImageIndex === -1){
                                    ok=false;
                                    reject(`no gi_cross for dataset ${datasetName} - cannot use cross base image`);
                                }
                                break;
                        }
                        if(ok){
                            // console.log("emitting job");
                            // console.log(`dataset:${datasetName}`);
                            // console.log(`baseImageIndex:${baseImageIndex}`);
                            // console.log(`mapsToProduce:${mapsToProduce}`);
                            // console.log(`doAlign:${doAlign}`);
                            this.#io.emit('process-data-set',datasetName,baseImageIndex,mapsToProduce,!!doAlign,(payload)=>{
                                // console.log(`callback ran`);
                                console.log(payload);
                                if(payload.status){
                                    resolve();
                                }else{
                                    reject(payload.msg);
                                }
                            });
                        }
                    }else{
                        reject(`unknown base image :${baseImage} - possible base images are ${this.possibleBaseImages.join(', ')}`);
                    }
                }else{
                    if(mapsNamedCorrectly){
                        reject(`insufficient input data  for set "${datasetName}" to make the following maps: ${impossibleMaps.join(', ')} - can only produce the following ${this.datasets[index].possibleMaps.join(', ')}`);
                    }else{
                        reject(`mapsToProduce named incorrectly, unknown maps include :${incorrectNames.join(', ')} - permissible maps names are: ${this.allPossibleMaps.join(', ')}`);
                    }
                }
            }else{
                reject(`unknown dataset: ${datasetName} - could not find in array of available datasets`);
            }

        });
    }

    done(){
        this.#io.disconnect();
    }
}

module.exports = ImageProcessor;