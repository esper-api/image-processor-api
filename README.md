# Esper Image Processor API

The API wrapper for the Image processor exposes all the functionality of the image processor in only a few functions. 

The module itself exports a class you must instantiate
```js
const ImageProcessorClass = require('./imageProcessor');

let ImageProcessor = new ImageProcessorClass();
```
The contructor for this class takes two optional arguments
```js
//ImageProcessorMasterIP = 192.168.11.1  - the master IP address set on VLAN 11 
//onConnectedCallback = ()=>{}   <- function is run when connection to master is established
let ImageProcessor = ImageProcessorClass(ImageProcessorMasterIP,onConnectedCallback);


//for Example:
let ImageProcessor = ImageProcessorClass('172.16.0.70',()=>{console.log("connected to Image Processor Master")})
// this allows you to point your script at the DHCP assigned 
// address of the Image Processor Master if the computer you are running it 
// from does not have a 192.168.11.0/24 address on VLAN 11 
```

## Datasets
The Image Processor reports the datasets it has available. These are populated onto the `ImageProcessor` class.
```js
//dataset array at : ImageProcessor.datasets
for (var i = 0; i < ImageProcessor.datasets.length; i++) {
    console.log(`dataset Name: ${ImageProcessor.datasets[i].name} at index: ${i}`);
}
```
Datasets are periodically emitted by the Image Processor. This can be triggered by various events, including updates to the filesystem. 
You can register a callback function to run when these updates come in.

```js
ImageProcessor.registerDatasetUpdateCallback((updatedDatasets)=>{
    console.log(`Dataset update occurred: now have ${updatedDatasets.length} datasets`);
})
```

Each dataset has many properties associated with it. These can be explored by printing them out to the console, but the most important ones for scripting are:
```js
dataset.name  // string - name of the dataset directory 
dataset.availableBaseImages = [Array], // array of strings - options are 'parallel','cross' and 'neutral'
dataset.possibleMaps: [Array],  // array of strings  - options are ['parallel','neutral','cross','normal','specular','tangent']

// note possible maps are determined by what information is in the dataset, not all maps can be produced from all datasets
```

## Processing data
data is processed with a call to `ImageProcessor.processDataset(...)`
This function has a number of optional arguments. It returns a promise that resolves when the dataset has finished processing.
```js
ImageProcessor.processDataset(
    datasetName,  // required - this is the name of the dataset directory
    baseImage,   //optional - defaut value 'parallel'  - options 'parallel','cross' or 'neutral'
    doAlignBool, //optionnal - default true
    mapsToProduce //optional - produce all maps - ['parallel','neutral','cross','normal','specular','tangent']
)

// for example, to process the first available dataset on the Image Processor 
if(ImageProcessor.datasets.length > 0 ){
    
    //get the name of the first available dataset
    let datasetNameToProcess = ImageProcessor.datasets[0].name; 
    
    //call processDataset and accept defaults (base image is parallel, do alignment and produce all maps), 
    ImageProcessor.processDataset(datasetNameToProcess)
        .then(()=>{
            console.log(`Dataset ${datasetNameToProcess} finished!`);
        })
        .catch((e)=>{
            console.log(`Error occured: ${e}`);
        })
}
```

Providing all arguments:
```js
if(ImageProcessor.datasets.length > 0 ){
    
    //get the name of the first available dataset
    let datasetNameToProcess = ImageProcessor.datasets[0].name; 
    
    //call processDataset and accept defaults (base image is parallel, do alignment and produce all maps), 
    ImageProcessor.processDataset(
        datasetNameToProcess,
        'neutral',   //use the neutral as the base image
        false,       // dont align images
        ['normal','tangent']  // only produce normal and tangent maps
    )
        .then(()=>{
            console.log(`Dataset ${datasetNameToProcess} finished!`);
        })
        .catch((e)=>{
            console.log(`Error occured: ${e}`);
        })
}
```